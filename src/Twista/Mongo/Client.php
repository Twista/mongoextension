<?php

namespace Twista\Mongo;

/**
 * @author Michal Haták <me@twista.cz>
 */
class Client {

    /** @var \MongoClient */
    private $client;

    /** @var  \MongoDB */
    private $db;

    /**
     * @param $dsn string
     * @param $dbName string
     */
    public function __construct($dsn, $dbName) {
        $this->client = new \MongoClient($dsn);
        $this->db = $this->client->selectDB($dbName);
    }

    /**
     * @return \MongoClient
     */
    public function getClient() {
        return $this->client;
    }

    /**
     * @return \MongoDB
     */
    public function getDB() {
        return $this->db;
    }

    /**
     * @param $collectionName string
     * @return \MongoCollection
     */
    public function getCollection($collectionName) {
        return $this->{$collectionName};
    }

    public function createDBRef($collectionName, $id){
        return $this->db->createDBRef($collectionName, $id);
    }

    public function getDBRef(array $arr){
        return $this->db->getDBRef($arr);
    }

    /**
     * @param $name
     * @return \MongoCollection
     */
    function __get($name) {
        return $this->db->{$name};
    }


} 