<?php

namespace Twista\Mongo\DI;

use Nette\Configurator;
use Nette\DI\Compiler;
use Nette\DI\CompilerExtension;
use Nette\InvalidArgumentException;

/**
 * @author Michal Haták <me@twista.cz>
 */
class MongoExtension extends CompilerExtension {

    private $defaults = array(
        "dsn" => "mongodb://localhost:27017",
        "dbName" => false
    );

    public function loadConfiguration() {

        $config = $this->getConfig($this->defaults);

        if(!$config["dbName"]){
            throw new InvalidArgumentException("Please specify extension configuration in your config file. 'dbName' missing");
        }

        $builder = $this->getContainerBuilder();

        $builder->addDefinition($this->prefix("mongo"))
            ->setClass('Twista\Mongo\Client', array(
                "dsn" => $config["dsn"],
                "dbName" => $config["dbName"]
            ));
    }

    /**
     * @param \Nette\Configurator $config
     */
    public static function register(Configurator $config)
    {
        $config->onCompile[] = function ($config, Compiler $compiler) {
            $compiler->addExtension('mongo', new MongoExtension());
        };
    }


} 