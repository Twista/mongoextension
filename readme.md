Twista\MongoExtension
=====

Requirements
------------

- [Nette Framework](https://github.com/nette/nette)
- [MongoDB](http://www.mongodb.org/)
- [php Mongo extension](http://php.net/manual/en/mongo.installation.php)


Installation
------------

download via composer

```sh
$ composer require twista/mongoextension:@dev
```

enable the extension using your neon config.

```yml
extensions:
    mongo: Twista\Mongo\DI\MongoExtension
```

configure extension


```yml
mongo:
    dsn: "mongodb://localhost:27017"
    dbName: "my_db_name"

```


Usage
------------

specify dependency in you presenter, like a

```php
class HomepagePresenter extends BasePresenter
{

    /** @var \Twista\Mongo\Client */
    private $client;

    public function __construct(Twista\Mongo\Client $client){
        parent::__construct();

        $this->client = $client;
    }
}
```

then you can use `Client->getCollection($collectionName)` or magic get `$client->collectionName` to receive instance of
[MongoCollection](http://php.net/manual/en/class.mongocollection.php) class.

you can also get native [MongoDB class](http://php.net/manual/en/class.mongodb.php) or [MongoClient class](http://php.net/manual/en/class.mongoclient.php) via methods
`$client->getDB()` and `$client->getClient()`

you can use also few shortcuts, like as ‘$mdb->createRef($collectionName, $mongoId)‘ and ‘$mdb->getDBRef(($arr)‘







